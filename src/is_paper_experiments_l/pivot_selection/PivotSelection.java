/*******************************************************************************
 * Copyright (c) 2013, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_l.pivot_selection;

import it.cnr.isti.vir.clustering.Centroids;
import it.cnr.isti.vir.clustering.KMeans;
import it.cnr.isti.vir.features.AbstractFeature;
import it.cnr.isti.vir.features.Floats;
import it.cnr.isti.vir.global.Log;
import it.cnr.isti.vir.similarity.ISimilarity;
import it.cnr.isti.vir.util.RandomOperations;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;

/**
 * It uses the VIR library.https://github.com/ffalchi/it.cnr.isti.vir
 * Various pivot selection from a list of CartesianPoint
 * @author Lucia
 *
 */
public class PivotSelection {
	
	//RANDOM SELECTION
	/**
	 * Select k random pivots from the data 
	 * @param data  data points
	 * @param k number of pivots 
	 * @return The list of pivots 
	 */
	public static List<CartesianPoint> random(List<CartesianPoint> data, int k) {
		List<CartesianPoint> outPivots=new ArrayList();
		int n_objects=data.size();
		int[] ids_pivots=RandomOperations.getDistinctInts(k,n_objects);
		for(int i1=0; i1<ids_pivots.length; i1++) 
			outPivots.add(data.get(ids_pivots[i1]));
		return outPivots;
	}
	
	
	
	/**
	 * Select k pivots using FFT . Just case K=2 is now working using sequential scan!
	 * @param data  data points
	 * @param k number of pivots 
	 * @return The list of pivots 
	 * @throws Exception 
	 */
	public static List<CartesianPoint> fft(List<CartesianPoint> data, Metric<CartesianPoint> metric, int k) throws Exception  {
		List<CartesianPoint> outPivots=new ArrayList();
		int n_objects=data.size();
		if(k==2) {
			int firstpivot=RandomOperations.getInt(0, n_objects);
			CartesianPoint p1=data.get(firstpivot);
			CartesianPoint p2=p1;
			outPivots.add(p1);
			double maxdist=0;
			for(CartesianPoint point: data) {
				double dist=metric.distance(point, p1);
				if(dist>maxdist )
					p2=point;
			}
			outPivots.add(p2);
		}
		else 
			throw new Exception("fft is not still working for selecting k>2 pivots");

		return outPivots;
	}

	/**
	 * Select k pivots using k-means/k-medoids from a list of CartesianPoint <br>
	 * It uses VIR library.
	 * @param data 
	 * @param k number of pivots 
	 * @param medoids if true, the pivots are selected from the data 
	 * @param outpath path for saving the k-means output
	 * @param  maxNObjs maximum number of object to be used in the kmeans , set to -1 to use all the data objects
	 * @param maxMinPerIteration 
	 * @param nIterations 
	 * @param distRedThr 
	 * @return The list of pivots 
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
		public static List<CartesianPoint> kmeansFloats(List<CartesianPoint> data, int k,boolean medoids,String outpath, int maxNObjs,ISimilarity vir_metric, double distRedThr, int nIterations, int maxMinPerIteration) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InterruptedException, IOException {
		List<CartesianPoint> outPivots=new ArrayList();
		ArrayList<AbstractFeature> virList =transformCartesianPoint_to_VIRfloatPoint(data);
		Floats[] virOut=(Floats[]) kmeans(virList, k,medoids,outpath, maxNObjs,vir_metric, distRedThr, nIterations, maxMinPerIteration);
		for (Floats ff: virOut) {
			outPivots.add(new CartesianPoint(ff.values));
			//	}
		}
		return outPivots;
	}
	/**
	 * transform a list of CartesianPoint into a list of VIR Floats feature
	 * @param data data(List of CartesianPoint)
	 * @return ArrayList of VIR Floats feature
	 */
	private static ArrayList<AbstractFeature> transformCartesianPoint_to_VIRfloatPoint(
			List<CartesianPoint> data) {
		ArrayList<AbstractFeature> virList = new ArrayList();
		for(CartesianPoint point: data) {
			double[] d_point=point.getPoint();
			int d_point_length=d_point.length;
			float[] f_point=new float[d_point_length];
			for(int i1=0; i1<d_point_length; i1++) 
				f_point[i1]=(float) d_point[i1];
			Floats f=new Floats(f_point);
			virList.add(f);
		}
		return virList;
	}



	/**
	 * Select k pivots using k-means/k-medoids from a list of VIR feature using the metric vir_metric
	 * @param list data points 
	 * @param k number of pivots 
	 * @param medoids if true, the pivots are selected from the data 
	 * @param outpath path for saving the k-means output
	 * @param  maxNObjs maximum number of object to be used in the kmeans , set to -1 to use all the data objects
	 * @param vir_metric metric used to compare the VIR feature. 
	 * @return The list of pivots 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private static AbstractFeature[] kmeans(ArrayList<AbstractFeature> list, int k, boolean medoids, String outpath,int maxNObjs, ISimilarity vir_metric,double distRedThr, int nIterations, int maxMinPerIteration) throws InterruptedException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		//	String outAbsolutePath = PropertiesUtils.getAbsolutePath(prop, "kMeans.outFileName");
			
		//	Class fClass = vir_metric.getRequestedFeaturesClasses().getRequestedClass();
		//	Class fGroupClass = null;
					
			double minDistortion = Double.MAX_VALUE;
			File bestOutFile = null;
			Centroids centroids = null;
			
			for ( int i=0; i<nIterations; i++ ) {
				Log.info("***ITER  " + i + " of "+nIterations+"***");
				KMeans<AbstractFeature> kmeans = new KMeans<AbstractFeature>(list, vir_metric);
				if ( medoids == true ) kmeans.setIsMedoid(medoids);
				Log.info("Setting medoid to " + medoids );
				kmeans.setDistRedThr(distRedThr);
				kmeans.setMaxMinPerIteration(maxMinPerIteration);
				kmeans.kMeansPP(k);
				kmeans.runAlgorithm(null);
				
				String fName = outpath+"kmeans_"+"medoid-"+medoids+"_" +kmeans.getDistortion()+ ".dat";
				File file = new File(fName);
				if ( kmeans.getDistortion() < minDistortion ) {
					minDistortion = kmeans.getDistortion();
					bestOutFile = file;
					centroids = new Centroids(kmeans.getCentroids(true));
				}
				
				System.out.println("Writing to " + fName);

			}
	
		return centroids.getCentroids();
	}
	
	

	
	
	
}
