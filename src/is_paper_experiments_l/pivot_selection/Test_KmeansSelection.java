/*******************************************************************************
 * Copyright (c) 2013, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_l.pivot_selection;

import is_paper_experiments_l.FileUtil;
import it.cnr.isti.vir.similarity.ISimilarity;
import it.cnr.isti.vir.similarity.metric.FloatsL2Metric;

import java.util.List;

import testloads.TestLoad;
import testloads.TestLoad.SisapFile;
import dataPoints.cartesian.CartesianPoint;

/**
 * @author Lucia
 *
 */
public class Test_KmeansSelection {
public static void main(String[] args) throws Exception {
	boolean medoids=false; //true for k-means, false for kmedoids
	
	int k=2; //set number of pivots
	String outpath="MATLAB/LuciaEXP/";		

	int maxNObjs=50000; //maximum number of object to be used in the kmeans , set to -1 to use all the data objects
	List<CartesianPoint> data=null;
	
	System.out.println("Using SISAP nasa dataset");
	SisapFile sFile= TestLoad.SisapFile.nasa;
	TestLoad tl = new TestLoad(sFile);
	data = tl.getDataCopy();
	List<CartesianPoint> referencePoints;

	if(medoids)
		System.out.println("***running kmedoid***");
	else
		System.out.println("***running kmeans***");
	double distRedThr= 0.000001; //VIR kmeans parameter
	int nIterations=1; //k-means is computed nIterations times and the solution with minimum distortion is considered 
	int maxMinPerIteration=120;//VIR kmeans parameter: max minute for iteration
	
	ISimilarity vir_metric=new FloatsL2Metric();; //VIR class to be used for k-means
	
	//
	
	referencePoints=PivotSelection.kmeansFloats(data,k,medoids,outpath,maxNObjs,vir_metric,distRedThr,nIterations,maxMinPerIteration);
	double[] p1=referencePoints.get(0).getPoint();
	double[] p2=referencePoints.get(1).getPoint();
	System.out.println("first reference point: "+FileUtil.doubleArrayToString(p1));
	System.out.println("second reference point: "+FileUtil.doubleArrayToString(p2));
}
}
