package is_paper_experiments_l.binary_tree_search_l_NOT_WORKING;


import is_paper_experiments_l.binary_partitions_l_NOT_WORKING.BinaryPartitionFactoryL;
import is_paper_experiments_l.binary_partitions_l_NOT_WORKING.PartitionGivenRefs;
import is_paper_experiments_r.binary_partitions.BinaryPartition;
import is_paper_experiments_r.binary_tree_search.BinaryExclusion;
import is_paper_experiments_r.binary_tree_search.BinaryExclusionFactory;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

/**
 * orig file was modified
 * @author Richard Connor
 *
 * @param <T>
 *            the type of the data to be searched
 */
public class HyperplaneTreeL<T> extends BinaryExclusionFactory<T, T, Double> {

	private BinaryPartitionFactoryL<T> partitionStrategy;
	private boolean fourPointExclusion;
	private T piv1;
	private T piv2;


	private class GHExclusion extends BinaryExclusion<T, T, Double> {

		// private List<T> initialData;
		private List<T> leftList;
		private List<T> rightList;
		T leftPivot;
		T rightPivot;
		double lrPivotDist;
		double crLeft, crRight, irLeft, irRight;

		@SuppressWarnings("synthetic-access")
		GHExclusion(List<T> initialData) {
			assert initialData != null : "data is null";
			assert initialData.size() > 0 : "data size is zero";

			this.leftList = new ArrayList<>();
			this.rightList = new ArrayList<>();

			this.leftPivot = piv1;
			this.rightPivot = piv2;

			BinaryPartition<T> pivots = HyperplaneTreeL.this.partitionStrategy.getPartition(initialData, piv1,piv2);
//
		
			List<T> remainingData = pivots.getData();
			this.lrPivotDist = HyperplaneTreeL.this.metric.distance(
					this.leftPivot, this.rightPivot);
			this.irLeft = Double.MAX_VALUE;
			this.irRight = Double.MAX_VALUE;
//
			for (T item : remainingData) {
				double d1 = HyperplaneTreeL.this.metric.distance(item,
						this.leftPivot);

				double d2 = HyperplaneTreeL.this.metric.distance(item,
						this.rightPivot);
				if (d1 < d2) {
					this.leftList.add(item);
					this.crLeft = Math.max(this.crLeft, d1);
					this.irLeft = Math.min(this.irLeft, d1);
				} else {
					this.rightList.add(item);
					this.crRight = Math.max(this.crRight, d2);
					this.irRight = Math.min(this.irRight, d2);
				}
			}
		
			
		}

		

		@SuppressWarnings("synthetic-access")
		@Override
		public ExclusionTest getQueryInfo(T query, double threshold,
				Double queryContext) {

			ExclusionTest res = new ExclusionTest(query) {

				@Override
				public Double getLeftQueryContext() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public Double getRightQueryContext() {
					// TODO Auto-generated method stub
					return null;
				}
			};

			double d1 = HyperplaneTreeL.this.metric.distance(query,
					this.leftPivot);
			if (d1 < threshold) {
				res.addResult(this.leftPivot);
			}
			if (this.rightPivot != null) {
				double d2 = HyperplaneTreeL.this.metric.distance(query,
						this.rightPivot);
				if (d2 < threshold) {
					res.addResult(this.rightPivot);
				}

				if (HyperplaneTreeL.this.fourPointExclusion) {
					if ((d1 * d1 - d2 * d2) / this.lrPivotDist >= 2 * threshold) {
						res.setExcludeLeft();
					}
					if ((d2 * d2 - d1 * d1) / this.lrPivotDist > 2 * threshold) {
						res.setExcludeRight();
					}
				} else {
					if ((d1 - d2) >= 2 * threshold) {
						res.setExcludeLeft();
					}
					if ((d2 - d1) > 2 * threshold) {
						res.setExcludeRight();
					}
				}
				if (d1 + threshold < this.irLeft) {
					res.setExcludeLeft();
				}
				if (d1 - threshold > this.crLeft) {
					res.setExcludeLeft();
				}
				if (d2 + threshold < this.irRight) {
					res.setExcludeRight();
				}
				if (d2 - threshold > this.crRight) {
					res.setExcludeRight();
				}

			}
			return res;
		}

		@Override
		public List<T> leftData() {
			return this.leftList;
		}

		@Override
		public List<T> rightData() {
			return this.rightList;
		}

		@Override
		public int storedDataSize() {
			return this.rightPivot == null ? 1 : 2;
		}

		@Override
		public T getLeftCreationContext() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public T getRightCreationContext() {
			// TODO Auto-generated method stub
			return null;
		}

	}

	/**
	 * Creates a new Hyperplane tree binary exclusion strategy
	 * 
	 * @param metric
	 *            the metric to be used
	 */
	public HyperplaneTreeL(Metric<T> metric) {
		super(metric);
		this.setPartitionStrategy(new PartitionGivenRefs<T>(metric));
		System.out.println("this h tree given refs");
	}

	

	@Override
	public BinaryExclusion<T, T, Double> getExclusion(List<T> data,
			T context) {
		return new GHExclusion(data);
	}

	
	/**
	 * @param partitionStrategy
	 *            the strategy to be used when creating partitions
	 */
	public void setPartitionStrategy(BinaryPartitionFactoryL<T> partitionStrategy) {
		this.partitionStrategy = partitionStrategy;
	}

	/**
	 * @param fourPoint
	 *            whether Hilbert Exclusion is to be used or not
	 */
	public void setFourPoint(boolean fourPoint) {
		this.fourPointExclusion = fourPoint;
	}

	@Override
	public String getName() {
		return "ght given pivots test";
	}
	
	/**
	 * @param piv1 the piv1 to set
	 */
	public void setPiv1(T piv1) {
		this.piv1 = piv1;
	}
	
	/**
	 * @param piv2 the piv2 to set
	 */
	public void setPiv2(T piv2) {
		this.piv2 = piv2;
	}

}
