/*******************************************************************************
 * Copyright (c) 2013, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

package is_paper_experiments_l.pca;

import java.util.List;

import org.ejml.data.DenseMatrix64F;
import org.ejml.factory.DecompositionFactory;
import org.ejml.factory.SingularValueDecomposition;
import org.ejml.ops.SingularOps;

import dataPoints.cartesian.CartesianPoint;

public class PCAviaSVD {
      
    /** centered data matrix**/
    DenseMatrix64F centeredX;
    
    /** number of point**/
    int n;
    
    /** number of dim**/
    int d;
    
    /** Vector of empirical means.   */
    double[] means;
   
    
    
     /**
     * Constructor given a double[][] data matrix 
     * @param dataMatrix Set of all data vectors, one vector per column
     * @throws Exception
     */
    public PCAviaSVD(double[][] dataMatrix) throws Exception {//dataMatrix=[x1|x2|..|xN], xi  D-dim
         d=dataMatrix.length;
         n=dataMatrix[0].length;
        
        //evaluating means
        means = new double[d];
        System.out.print("PCA is evaluating mean...");
        for (int i = 0; i < n; i++)
            for (int j = 0; j < d; j++)
                means[j] += dataMatrix[j][i];
        
        for (int i = 0; i < d; i++)
            means[i] /= n;
        
        System.out.println("done");
        //data centering
        centeredX = new DenseMatrix64F(d,n);
        for(int i1=0; i1<d;i1++)
            for(int i2=0;i2<n; i2++)
            	centeredX.set(i1, i2, dataMatrix[i1][i2]-means[i1]);
    }
    
    
	


	/**
	 * Constructor for data stored in a List of CartesianPoint  
	 * @param data
	 * @throws Exception 
	 * 
	 */
	public PCAviaSVD(List<CartesianPoint> data) throws Exception {
		//trandform a  List<CartesianPoint> into a DenseMatrix64F<br>
		 n=data.size();
		 d=data.get(0).getPoint().length;
		centeredX=new DenseMatrix64F(d ,n);//mat dxn// each coumn is a data point is a 
		means= new double[d];
		
		for(int i2=0; i2<n; i2++) {
			double[] p=data.get(i2).getPoint();
			for(int i1=0; i1<d; i1++) {
				double pi1=p[i1];
				centeredX.set(i1, i2, pi1);
				means[i1]+=pi1;
			}
		}
		for(int i1=0; i1<d;i1++)
			means[i1]=means[i1]/n;
		//center the matrix
		for(int i1=0; i1<d;i1++)
            for(int i2=0;i2<n; i2++)
            	centeredX.set(i1, i2, centeredX.get(i1, i2)-means[i1]);
		
	   // computePCA();
	}
	
	
	/**
	 * Constructor for data stored in a DenseMatrix64F 
	 * @throws Exception 
	 * 
	 */
	public PCAviaSVD(DenseMatrix64F X) throws Exception {
		//initialize mean and centeredX
		 n=X.numCols;
		 d=X.numRows;
		centeredX=new DenseMatrix64F(d ,n);//mat dxn// each coumn is a data point is a 
		means= new double[d];
		
		for(int i1=0; i1<d; i1++) {
			for(int i2=0; i2<n; i2++) {
				double value=X.get(i1, i2);
				means[i1]+=value;
			}
			means[i1]/=n;
		}
	 //center the matrix
		for(int i1=0; i1<d;i1++)
            for(int i2=0;i2<n; i2++)
            	centeredX.set(i1, i2, X.get(i1, i2)-means[i1]);
		
	}
	
	/**
	 * compute PCA via svd
	 * 
	 * @throws Exception 
	 * 
	 */
	public  PrincipalComponents computePCA() throws Exception {

		//evaluating SVD
	    System.out.println("PCA is evaluating principal components via SVD...");
	    System.out.println("Original dim:" +d);
	    /*
	     *  The singular value decomposition (SVD) of a matrix is defined as: A = U * W * V'
	     *  where A is d by n, and U and V are orthogonal matrices, and W is a diagonal matrix.
	     *  The dimension of U,W,V depends if it is a compact SVD or not. 
	     *  If not compact then U is d by d, W is d by n, V is n by n.* 
	     * *Note* that the ordering of singular values is not guaranteed, 
	     * unless done so by a specific implementation. The singular values can be put into descending 
	     * order while adjusting U and V using SingularOps.descendingOrder(org.ejml.data.DenseMatrix64F, boolean, org.ejml.data.DenseMatrix64F, org.ejml.data.DenseMatrix64F, boolean) SingularOps.descendingOrder()}. 
	     */
	  //the square roots of the eigenvalues are the singular values
	    SingularValueDecomposition<DenseMatrix64F> svd=DecompositionFactory.svd(d, n, true, false, false);//compute only U
	    long start = System.currentTimeMillis();
	    if(!svd.decompose(centeredX))
	        throw new Exception("SVD failed");
	    
	    System.out.println("Time: " + (System.currentTimeMillis()-start)/1000 );
	    
	    DenseMatrix64F U=svd.getU(null, true);//     /** Matrix of principal components (principal components are stored in row).  */
	    DenseMatrix64F W= svd.getW(null);
	    //sorting principal components
	    SingularOps.descendingOrder(U,true , W, null, false);
	    
	    int s=Math.min(n, d);
	    double[] squaredSingularValues= new double[s];
	    
	    for(int i1=0; i1<s; i1++) {
	       	double val=W.get(i1,i1);
	       	squaredSingularValues[i1]=val*val;
	    }
	      
	    PrincipalComponents pc=new PrincipalComponents(U, means,squaredSingularValues);
	    
	    System.out.println("done");
		
		return pc;
	}

	


    
    
}

