/*******************************************************************************
 * Copyright (c), Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

package is_paper_experiments_l.pca;

import is_paper_experiments_l.MathUtil;

import java.util.ArrayList;
import java.util.List;

import org.ejml.data.DenseMatrix64F;

import dataPoints.cartesian.CartesianPoint;

public class PrincipalComponents {

	/*original dim*/
	private int d;
	/*original reduced dim*/
	private int rd;
	/* pca matrix: principal components related to non zero eigenvalue are stored in row*/
	private double[][] eigenVectors;
	/* non zero*/
	private double[] eigenValues;
	/*means to be subtracted before rotating the points*/
	private double[] means;

	static final double NUMERIC_ZERO=Math.pow(10, -16);
/**
	 * @param ut Matrix of principal components (principal components are stored in row)
	 * @param means
	 * @param allEigenValues
	 */
	public PrincipalComponents(DenseMatrix64F ut, double[] means,
			double[] allEigenValues) {
		this.means=means;
		int count=0;
		for(int i1=0; i1<allEigenValues.length;i1++) {
			double val=allEigenValues[i1];
			if(val > NUMERIC_ZERO)
				count++;
		}
		rd=count;
		d=ut.numRows;
		eigenValues= new double[rd];
		for(int i1=0; i1<rd;i1++) 
			eigenValues[i1]=allEigenValues[i1];
	
		
		eigenVectors=new double[rd][d];
		
		for(int i1=0; i1<rd;i1++)
            for(int i2=0; i2<d;i2++)
            	eigenVectors[i1][i2]= ut.get(i1, i2);
	}
	
	/**
	 * @return the eigenValues
	 */
	public double[] getEigenValues() {
		return eigenValues;
	}
	
	/**
	 * @return the eigenVectors
	 */
	public double[][] getEigenVectors() {
		return eigenVectors;
	}
	
/**
 * @return the means
 */
public double[] getMeans() {
	return means;
}

/**
 * percentage of the total variance explained by explainded by the i-th principal component
 * @param i value from 1 to eigenValues.length (included)
 * @return
 */
public double explainedVariance(int i) {
	if(i>eigenValues.length|| i<1) return 0;
	double all_variance=MathUtil.sum(eigenValues);	
	double explained_variance=eigenValues[i-1]/all_variance*100;
	return explained_variance;
}

/**
 * @param data
 * @return
 */
public List<CartesianPoint> centerANDrotate(List<CartesianPoint> data) {
	List<CartesianPoint> centeredRotatedData=new ArrayList<CartesianPoint>();

	for(CartesianPoint cp: data) {
		double[] p=cp.getPoint();	
		double[] rp=new double[d];		
		for (int i1=0; i1<rd; i1++) {
			double sum=0;
			for (int i2=0; i2<d; i2++) {
				double val=p[i2]-means[i2];
				sum+=eigenVectors[i1][i2]*val;
			}
			rp[i1]=sum;
			}
		centeredRotatedData.add(new CartesianPoint(rp));
		
	}
	return centeredRotatedData;
}
	
//TODO get reduced
	
}
