package is_paper_experiments_l.binary_partitions_l_NOT_WORKING;

import is_paper_experiments_r.binary_partitions.BinaryPartition;

import java.util.List;

import coreConcepts.Metric;

public abstract class BinaryPartitionFactoryL<T> {
	protected Metric<T> metric;


	protected BinaryPartitionFactoryL(Metric<T> metric) {
		this.metric = metric;
	}	
	public abstract BinaryPartition<T> getPartition(List<T> data, T givenRef1, T givenRef2);

}
