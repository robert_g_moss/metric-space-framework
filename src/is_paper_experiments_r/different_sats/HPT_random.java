package is_paper_experiments_r.different_sats;

import is_paper_experiments_r.n_ary_trees.SAT2ExclusionFactory;
import is_paper_experiments_r.util.Util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import coreConcepts.Metric;

public class HPT_random<T> extends SAT2ExclusionFactory<T> {

	protected boolean fourpoint;
	protected boolean fft;

	public static enum Arity {
		unlimited, log, fixed, binary
	}

	Arity arity;

	public HPT_random(Metric<T> metric, boolean fourPoint, Arity arity,
			boolean fft) {
		super(metric, false);
		assert arity != Arity.unlimited;
		this.fourpoint = fourPoint;
		this.fft = fft;
		this.arity = arity;
	}

	@Override
	protected List<T> getReferencePoints(List<T> data, T centre) {

		if (this.fft) {
			if (this.arity == Arity.binary) {
				return Util.getFFT(data, metric, 2);
			} else if (this.arity == Arity.fixed) {
				return Util.getFFT(data, metric, 4);
			} else if (this.arity == Arity.log) {
				return Util.getFFT(data, metric,
						Math.max(2, (int) Math.floor(Math.log(data.size()))));
			} else {
				throw new RuntimeException("bad arity");
			}
		} else {
			if (this.arity == Arity.binary) {
				return Util.getRandom(data, 2);
			} else if (this.arity == Arity.fixed) {
				return Util.getRandom(data, 4);
			} else if (this.arity == Arity.log) {
				return Util.getRandom(data,
						Math.max(2, (int) Math.floor(Math.log(data.size()))));
			} else {
				throw new RuntimeException("bad arity");
			}
		}
	}

	@Override
	protected boolean useFourPointProperty() {
		return this.fourpoint;
	}

	@Override
	protected boolean useSatProperty() {
		return false;
	}

	@Override
	public String getName() {
		return "HPT_" + (this.fft ? "fft_" : "random_") + this.arity
				+ (this.fourpoint ? "_f" : "");
	}

}
