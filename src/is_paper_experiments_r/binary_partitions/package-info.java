/**
 * various different mechanisms for choosing two reference points from a set, for use in binary hyperplane trees
 */
/**
 * @author Richard Connor
 *
 */
package is_paper_experiments_r.binary_partitions;