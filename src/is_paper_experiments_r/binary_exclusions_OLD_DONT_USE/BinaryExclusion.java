package is_paper_experiments_r.binary_exclusions_OLD_DONT_USE;

import java.util.ArrayList;
import java.util.List;

/**
 * @author newrichard
 *
 * @param <T>
 */
/**
 * 
 * 
 * This class (maybe should be an interface, but may start to include utility
 * functions) is the one available to a generic metric index implementation,
 * which allows the logical separation of creation, testing etc from the actual
 * exclusion mechanisms being used
 * 
 * use: an ExclusionFactory class is used by a generic data arrangement
 * mechanism (eg a binary search tree in the simplest case) to generate an
 * exclusion according to the given context, ie the data and metric available
 * 
 * the ExclusionFactory implementation also needs to know
 * 
 * @author Richard Connor
 *
 * @param <T>
 *            the type of data being searched
 */
public abstract class BinaryExclusion<T> {

	/**
	 * result of asking if either child node of a binary tree can be excluded
	 * from the search
	 * 
	 * @author Richard Connor
	 *
	 */
	public class ExclusionTest {

		private List<T> newResults;
		private boolean[] exclusions;
		public double utility;

		public ExclusionTest() {
			this.newResults = new ArrayList<>();
			this.exclusions = new boolean[2];
		}

		public void addResult(T result) {
			this.newResults.add(result);
		}

		/**
		 * @return
		 */
		public List<T> getResults() {
			return this.newResults;
		}

		public void setExcludeLeft() {
			this.exclusions[0] = true;
		}

		public void setExcludeRight() {
			this.exclusions[1] = true;
		}

		public boolean canExcludeLeft() {
			return this.exclusions[0];
		}

		public boolean canExcludeRight() {
			return this.exclusions[1];
		}
	}

	/**
	 * must be called before either excludeLeft or excludeRight at each node
	 * 
	 * @param m
	 */
	public abstract ExclusionTest getExclusions(T query, double threshold);

	/**
	 * 
	 * @return data to be stored in the left subtree, recursively
	 */
	public abstract List<T> leftData();

	/**
	 * 
	 * @return data to be stored in the right subtree, recursively
	 */
	public abstract List<T> rightData();

	public abstract int storedDataSize();

}
