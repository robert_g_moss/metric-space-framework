package is_paper_experiments_r.binary_exclusions_OLD_DONT_USE;

import is_paper_experiments_r.binary_exclusions_OLD_DONT_USE.BinaryExclusion.ExclusionTest;

import java.util.ArrayList;
import java.util.List;

import searchStructures.SearchIndex;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;

/**
 * 
 * This class builds a generic binary tree independently from the mechanism used
 * to split the data at each node, or to exclude child nodes from queries
 * 
 * this allows the same search structure to be used for lots of different index
 * types
 * 
 * @author Richard Connor
 *
 * @param <T>
 *            the type of the object being stored
 */
public class SearchTree<T> extends SearchIndex<T> {

	private class TreeNode {
		BinaryExclusion<T> e;
		List<T> nodeData;
		private int depth;
		TreeNode left;
		TreeNode right;

		@SuppressWarnings("synthetic-access")
		TreeNode(List<T> data, int depth) {
			this.e = SearchTree.this.exclusion.getExclusion(data);
			this.nodeData = new ArrayList<>();
			this.depth = depth;
			final List<T> leftData = this.e.leftData();
			if (leftData.size() > 0) {
				this.left = new TreeNode(leftData, depth + 1);
			}
			final List<T> rightData = this.e.rightData();
			if (rightData.size() > 0) {
				this.right = new TreeNode(rightData, depth + 1);
			}
		}

		public int cardinality() {
			int res = this.nodeData.size();
			res += e.storedDataSize();
			if (this.left != null) {
				res += this.left.cardinality();
			}
			if (this.right != null) {
				res += this.right.cardinality();
			}
			return res;
		}

		public int maxDepth() {
			int res = this.depth;
			if (this.left != null) {
				res = Math.max(this.depth, this.left.maxDepth());
			}
			if (this.right != null) {
				res = Math.max(this.depth, this.right.maxDepth());
			}
			return res;
		}

		public void thresholdSearch(T query, List<T> res, double t) {

			BinaryExclusion<T>.ExclusionTest excs = e.getExclusions(query, t);
			res.addAll(excs.getResults());
			if (this.left != null && !excs.canExcludeLeft()) {
				this.left.thresholdSearch(query, res, t);
			}
			if (this.right != null && !excs.canExcludeRight()) {
				this.right.thresholdSearch(query, res, t);
			}

		}

	}

	private TreeNode theTree;

	private BinaryExclusionFactory<T> exclusion;

	private CountedMetric<T> cm;

	/**
	 * creates a searchable index structure according to an exclusion strategy
	 * passed in
	 * 
	 * @param data
	 *            data to be searched
	 * @param e
	 *            the exclusion strategy
	 */
	public SearchTree(List<T> data, BinaryExclusionFactory<T> e) {
		// super call populates this.data, this.metric, and this.rand
		// if that's all it does we should probably change SearchIndex to an
		// interface
		super(data, e.getMetric());
		this.exclusion = e;
		this.theTree = new TreeNode(data, 0);
	}

	/**
	 * mostly for test purposes
	 * 
	 * @return the number of data items stored in the tree
	 */
	public int cardinality() {
		return this.theTree.cardinality();
	}

	/**
	 * @return the maximum depth of the tree
	 */
	public int depth() {
		return this.theTree.maxDepth();
	}

	@Override
	public String getShortName() {
		return this.exclusion.getName();
	}

	@Override
	public List<T> thresholdSearch(T query, double t) {
		List<T> res = new ArrayList<>();
		this.theTree.thresholdSearch(query, res, t);
		return res;
	}

}
