package is_paper_experiments_r.binary_exclusions_OLD_DONT_USE;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

/**
 * An exhaustive search mechanism which can be used to test and benchmark other
 * more sophisticated search mechanisms
 * 
 * @author Richard Connor
 *
 * @param <T>
 *            the type of the data being searched
 */
public class ExhaustiveSearchTree<T> extends BinaryExclusionFactory<T> {

	private class NoExclusion extends BinaryExclusion<T> {

		List<T> data;

		private NoExclusion(List<T> data) {
			this.data = data;
		}

		@Override
		public List<T> leftData() {
			if (this.data.size() > 1) {
				return this.data.subList(1, ((this.data.size() / 2) + 1));
			} else {
				return new ArrayList<>();
			}
		}

		@Override
		public List<T> rightData() {
			if (this.data.size() > 1) {
				return this.data.subList((this.data.size() / 2) + 1,
						this.data.size());
			} else {
				return new ArrayList<>();
			}
		}

		@Override
		public ExclusionTest getExclusions(T query, double threshold) {
			// in this case there is only a single exclusion datum which must be
			// present
			ExclusionTest res = new ExclusionTest();
			if (ExhaustiveSearchTree.this.metric.distance(this.data.get(0), query) < threshold) {
				res.addResult(this.data.get(0));
			}
			return res;
		}

		@Override
		public int storedDataSize() {
			return 1;
		}

	}

	/**
	 * @param metric
	 */
	public ExhaustiveSearchTree(Metric<T> metric) {
		super(metric);
	}

	@SuppressWarnings("synthetic-access")
	@Override
	public BinaryExclusion<T> getExclusion(List<T> data) {
		return new NoExclusion(data);
	}

	@Override
	public String getName() {
		return "exhaustive_tree_search";
	}

}
