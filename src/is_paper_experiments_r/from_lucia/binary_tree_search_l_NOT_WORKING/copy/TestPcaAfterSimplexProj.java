/*******************************************************************************
 * Copyright (c) 2016, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_r.from_lucia.binary_tree_search_l_NOT_WORKING.copy;


import is_paper_experiments_r.binary_tree_search.BinaryExclusionFactory;
import is_paper_experiments_r.binary_tree_search.ExhaustiveSearchTree;
import is_paper_experiments_r.binary_tree_search.SearchTree;
import is_paper_experiments_r.from_lucia.PrincipalComponents;
import is_paper_experiments_r.from_lucia.binary_partitions_l_NOT_WORKING.copy.PartitionGivenRefs;
import is_paper_experiments_r.from_lucia.pca.copy.PCAviaSVD;
import is_paper_experiments_r.from_lucia.pivot_selection.PivotSelection;

import java.util.ArrayList;
import java.util.List;

import supermetrics.SimplexND;
import testloads.TestContext;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;

/** NOT WORKING
 * @author Lucia
 *
 */
public class TestPcaAfterSimplexProj {
	public static void main(String[] args) throws Exception {
		int maxNObjs=30000;//maximum number of points to be considering in learning the pca matrix
		int noOfQueries = 1000;
		
		System.out.println("Using SISAP nasa dataset");
		TestContext tc = new TestContext(TestContext.Context.colors);
		
		////////////////////////////////////////////////////////
		Metric metric = tc.metric();
		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		tc.setSizes(noOfQueries, 0);
		
		List<CartesianPoint> dataOrig = tc.getData();	
		List<CartesianPoint> queryOrig = tc.getQueries();
		
		List<CartesianPoint> datapj=new ArrayList<CartesianPoint>();	//2-simplex projected data 
		List<CartesianPoint> querypj=new ArrayList<CartesianPoint>(); //2-simplex projected query 
		List<CartesianPoint> refpj=new ArrayList<CartesianPoint>();
		//select 2 random  reference objects
		//System.out.println("***running random (pivots are selected from the dataset)***");
		List<CartesianPoint> referencePoints= PivotSelection.random(dataOrig,2);
		//
		
		//projecting 2-simplex
		SimplexND<CartesianPoint> simplex = new SimplexND<>(2, metric, referencePoints);
	
		for(CartesianPoint p:dataOrig) {
			CartesianPoint apex=new CartesianPoint(simplex.formSimplex(p));
			datapj.add(apex);
		}
		for(CartesianPoint p:queryOrig) {
			CartesianPoint apex=new CartesianPoint(simplex.formSimplex(p));
			querypj.add(apex);
		}
		for(CartesianPoint r:referencePoints) {
			CartesianPoint apex=new CartesianPoint(simplex.formSimplex(r));
			refpj.add(apex);
		}
		////////*****PCA********//////////
		List data4PCA=datapj;
		
		//select a random subset of the data 
		if(dataOrig.size()>maxNObjs){
			System.out.println("Sececting "+maxNObjs+" from "+dataOrig.size()+" data objects");
			ArrayList<Integer> list=new ArrayList<Integer>();
			for(int i1=0; i1<dataOrig.size(); i1++) {
				list.add(i1);
			}
			java.util.Collections.shuffle(list);
			
			List<Integer> list2=list.subList(0, maxNObjs);
			
			data4PCA=new ArrayList ();
			for(Integer i1: list2) {
				data4PCA.add(datapj.get(i1));
			}
			
		}
		// end selection points/
		
		PCAviaSVD pca= new PCAviaSVD(data4PCA);
		is_paper_experiments_r.from_lucia.pca.copy.PrincipalComponents pc = pca.computePCA();
		
		
//		double[][] rotationMatrix=pc.getEigenVectors();//eigenVec stored in row 
		double[] eigenValues=pc.getEigenValues();
		
		System.out.println("number of eigenvalue greater than 10^-16: " +eigenValues.length);
		double explained_variance=pc.explainedVariance(1);
		System.out.println("percentage of the total variance explained by the 1st principal component: "+explained_variance);

		explained_variance=pc.explainedVariance(eigenValues.length);
		System.out.println("percentage of the total variance explained by the last ("+eigenValues.length+") principal component: "+explained_variance);
//		
		////////*****end PCA********//////////
		
		//Exhaustive search//
		System.out.println("*********************************");
		List<BinaryExclusionFactory<CartesianPoint, ?, ?>> excMechs = getMechanisms(tc, cm);
		cm.reset();
		for (BinaryExclusionFactory<CartesianPoint, ?, ?> mech : excMechs) {
			SearchTree<CartesianPoint, ?, ?> tree = new SearchTree<>(dataOrig, mech);
			System.out.println("mechanism: " + tree.getShortName());
			System.out.println("data size: " + tree.cardinality());
			List<CartesianPoint> res = new ArrayList<>();
			cm.reset(); // actually unnecessary but future-proofed!

			for (CartesianPoint q : queryOrig) {
				res.addAll(tree.thresholdSearch(q, tc.getThreshold()));
				// System.out.println(cm.reset());
			}
			System.out.println("results size " + res.size());
			System.out.println("mean distances per query: "
					+ (cm.reset() / noOfQueries));
			
		}
		
//		//Hyperplane tree  2-simplex proj//
		System.out.println("*********************************");
		List<BinaryExclusionFactory<CartesianPoint, ?, ?>> excMechsAfterSimplexProj = getMechanismsAfterSimplexProj(tc, cm,refpj);
		cm.reset();
		int count=0;
		for (BinaryExclusionFactory<CartesianPoint, ?, ?> mech : excMechsAfterSimplexProj) {
			count ++;
			
			SearchTreeL<CartesianPoint, ?, ?> tree = new SearchTreeL<>(datapj, mech);
			System.out.println("************* " +count+"a ********************");
			System.out.println("mechanism: " + tree.getShortName());
			System.out.println("data size: " + tree.cardinality());
			List<CartesianPoint> res = new ArrayList<>();
			cm.reset(); 

			for (CartesianPoint q : querypj) {
				res.addAll(tree.thresholdSearch(q, tc.getThreshold()));
				// System.out.println(cm.reset());
			}
			System.out.println("results size " + res.size());
			System.out.println("mean distances per query: "
					+ (cm.reset() / noOfQueries));
			
		}

		
		//Hyperplane tree PCA after 2-simplex proj//
		querypj=pc.centerANDrotate(querypj);
		datapj=pc.centerANDrotate(datapj);
		
				System.out.println("*********************************");
				List<BinaryExclusionFactory<CartesianPoint, ?, ?>> excMechsPCA = getMechanismsPCA(tc, cm);
				cm.reset();
				 count=0;
				for (BinaryExclusionFactory<CartesianPoint, ?, ?> mech : excMechsPCA) {
					count ++;
					
					SearchTreeL<CartesianPoint, ?, ?> tree = new SearchTreeL<>(datapj, mech);
					System.out.println("************* " +count+"b ********************");
					System.out.println("mechanism: " + tree.getShortName());
					System.out.println("data size: " + tree.cardinality());
					List<CartesianPoint> res = new ArrayList<>();
					cm.reset(); 
					
					for (CartesianPoint q : querypj) {
						res.addAll(tree.thresholdSearch(q, tc.getThreshold()));
						// System.out.println(cm.reset());
					}
					System.out.println("results size " + res.size());
					System.out.println("mean distances per query: "
							+ (cm.reset() / noOfQueries));
					
				}


	}
	
	
	/**
	 * @param tc
	 * @param cm
	 * @return
	 */
	private static List<BinaryExclusionFactory<CartesianPoint, ?, ?>> getMechanismsPCA(
			TestContext tc, CountedMetric<CartesianPoint> cm) {
		int count=0;
		System.out.println("******LEGEND*****");
		List<BinaryExclusionFactory<CartesianPoint, ?, ?>> res = new ArrayList<>();
		
		double[] px1= {-1,0};
		double[] px2= {1,0};
		double[] py1= {0,-1};
		double[] py2= {0,1};
		CartesianPoint cpy1=new CartesianPoint(py1);
		CartesianPoint cpy2=new CartesianPoint(py2);
		CartesianPoint cpx1=new CartesianPoint(px1);
		CartesianPoint cpx2=new CartesianPoint(px2);
		count++;
		
		//TEST a simple binary hyperplane tree (not necessarily balanced) obtained by considering an hyperplane on 2-dim euclidean space (obtained considering the 2-simplex projection determined by the two reference objects)  choosen by using  PCA on the 2-dim (simplex)  projected space.
		System.out.println(count+"b** HyperplaneTreeL after 2-simplex projection using 2 random pivots, PCA 1 comp hyp (using 4 point prop) ");
//				
		boolean fourPoint = true;
		final HyperplaneTreeL<CartesianPoint> hpt1 = new HyperplaneTreeL<>(cm);
		hpt1.setFourPoint(fourPoint);
		hpt1.setPiv1(cpy1);
		hpt1.setPiv2(cpy2 );
		hpt1.setPartitionStrategy(new PartitionGivenRefs<>(cm));
		res.add(hpt1);

		
		System.out.println(count+"** HyperplaneTreeL after 2-simplex projection using 2 random pivots, PCA 2 comp hyp (using 4 point prop) ");
		//		
		
		final HyperplaneTreeL<CartesianPoint> hpt2 = new HyperplaneTreeL<>(cm);
		hpt2.setFourPoint(fourPoint);
		hpt2.setPiv1(cpx1);
		hpt2.setPiv2(cpx1);
		hpt2.setPartitionStrategy(new PartitionGivenRefs<>(cm));
		res.add(hpt2);
		
		System.out.println("**********************");

		return res;
		
	}


	/**
	 * @param tc
	 * @param cm
	 * @param referencePoints
	 * @param pc
	 * @return
	 */
	private static List<BinaryExclusionFactory<CartesianPoint, ?, ?>> getMechanismsAfterSimplexProj(
			TestContext tc, CountedMetric<CartesianPoint> cm,
			List<CartesianPoint> referencePoints) {
		
		int count=0;
		System.out.println("******LEGEND*****");
		List<BinaryExclusionFactory<CartesianPoint, ?, ?>> res = new ArrayList<>();
		
		count++;
		
	 /*
	  * TEST binary hyperplane tree by considering the hyperplane determined by two randomly selected reference points ( i.e. considering the hyperplane of the  equidistant point to the ref.objects)
	  */
		System.out.println(count+"a** HyperplaneTreeL after 2-simplex projection using 2 random pivots (using 4 point prop) ");
		// add a HPT using the four point property and random reference point
		// selection
		boolean fourPoint = true;
		final HyperplaneTreeL<CartesianPoint> hpt1 = new HyperplaneTreeL<>(cm);
		hpt1.setFourPoint(fourPoint);
		hpt1.setPiv1(referencePoints.get(0));
		hpt1.setPiv2(referencePoints.get(1));
		hpt1.setPartitionStrategy(new PartitionGivenRefs<>(cm));
		res.add(hpt1);

		// add a HPT  usingrandom reference point
		// selection (no 4 point prop)
//		count++;
//		System.out.println(count+"** HyperplaneTreeL after 2-simplex projection using 2 random pivots");
//		fourPoint = false;
//		final HyperplaneTreeL<CartesianPoint> hpt1b = new HyperplaneTreeL<>(cm,referencePoints.get(0),referencePoints.get(1));
//		hpt1b.setFourPoint(fourPoint);
//		hpt1b.setPiv1(referencePoints.get(0));
//		hpt1b.setPiv2(referencePoints.get(1));
//		hpt1b.setPartitionStrategy(new PartitionGivenRefs<>(cm,referencePoints.get(0),referencePoints.get(1)));
//		res.add(hpt1b);
//
//		
		System.out.println("**********************");

		return res;
	}


	private static List<BinaryExclusionFactory<CartesianPoint, ?, ?>> getMechanisms(
			TestContext tc, Metric<CartesianPoint> cm) {
		List<BinaryExclusionFactory<CartesianPoint, ?, ?>> res = new ArrayList<>();

		// add exhaustive search to benchmark
		System.out.println("Exhaustive search");
		final ExhaustiveSearchTree<CartesianPoint> est = new ExhaustiveSearchTree<>(
				cm);
		res.add(est);
		return res;
	}
}
