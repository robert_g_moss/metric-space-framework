/*******************************************************************************
 * Copyright (c) 2013, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_r.from_lucia.binary_partitions_l_NOT_WORKING.copy;

import is_paper_experiments_r.binary_partitions.BinaryPartition;

import java.util.ArrayList;
import java.util.List;

import coreConcepts.Metric;

/**
 * @author Lucia
 *
 */
public class PartitionGivenRefs<T> extends BinaryPartitionFactoryL<T> {
	private T givenRef1;
	private T givenRef2;

//	/**
//	 * @param metric
//	 */
//	public PartitionGivenRefs(Metric<T> metric,T givenRef1, T givenRef2) {
//		super(metric);
//		this.givenRef1=givenRef1;
//		this.givenRef2=givenRef2;
//	}
	public PartitionGivenRefs(Metric<T> metric) {
		super(metric);
//		this.givenRef1=givenRef1;
//		this.givenRef2=givenRef2;
	}

	private class PartGivenRef extends BinaryPartition<T> {

		List<T> refs;

		/**
		 * @param data
		 * @param metric
		 */
		public PartGivenRef(List<T> data, Metric<T> metric) {
			super(data, metric);
		}

		@SuppressWarnings("synthetic-access")
		@Override
		protected void intialise() {
			this.refs = new ArrayList<>();

			T ref1 = givenRef1;
			this.refs.add(ref1);
			T ref2 = givenRef2;
			this.refs.add(ref2);
			
		}

		@Override
		public List<T> getReferencePoints() {
			return this.refs;
		}

		
		@Override
		public List<T> getData() {
			return this.data;
		}
		
	}

	

	
	@Override
	public BinaryPartition<T> getPartition(List<T> data, T givenRef1,
			T givenRef2) {
		this.givenRef1 = givenRef1;
		this.givenRef2 = givenRef2;
		return new PartGivenRef(data, this.metric);
	}


		
	
	
		
	}

