/*******************************************************************************
 * Copyright (c) 2013, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_r.from_lucia.pivot_selection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import dataPoints.cartesian.CartesianPoint;

/**
 * @author Lucia
 *
 */
public class FileUtil {
	public static void saveFile(File file, String text) {
		System.out.println("Saving "+file.getAbsolutePath());
		saveFile(file,text,false);
	}

	public static void saveFile(File file, String text, boolean append) {
	    try (FileWriter fw = new FileWriter(file, append);
	        BufferedWriter bw = new BufferedWriter(fw)) {
	        bw.write(text);
	        bw.close();
	    } catch (IOException e) {
	        throw new IllegalArgumentException(e);
	    }
	}
	public static String doubleArrayToString(double[][] m) {
		return doubleArrayToString(m, ",");
	}
	public static String doubleArrayToString(double[][] m, String colDelimiter) {
		StringBuilder str = new StringBuilder(m.length * m[0].length);
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length - 1; j++) {
				str = str.append(Double.toString(m[i][j]));
				str = str.append(colDelimiter);
			}
			str = str.append(Double.toString(m[i][m[i].length - 1]));
			str = str.append("\n");
		}
		return str.toString();
	}
	public static String doubleArrayToString(double[] m) {
		return doubleArrayToString(m, ",");
	}
	public static String doubleArrayToString(double[] m, String colDelimiter) {
		StringBuilder str = new StringBuilder(m.length );
		for (int i = 0; i < m.length-1; i++) {
				str = str.append(Double.toString(m[i]));
				str = str.append(colDelimiter);
		}
		str = str.append(Double.toString(m[m.length-1]));
		str = str.append("\n");

		return str.toString();
	}

	public static String ListCartesianPointToString(
			List<CartesianPoint> referencePoints) {
		return ListCartesianPointToString(referencePoints, ",");
	}
	/**
	 * @param referencePoints
	 * @return
	 */
	public static String ListCartesianPointToString(
			List<CartesianPoint> referencePoints,String colDelimiter) {
		int nref=referencePoints.size();
		int dim=referencePoints.get(0).getPoint().length;
		StringBuilder str = new StringBuilder(nref * dim);
		for(CartesianPoint ref: referencePoints) {
			double[] m=ref.getPoint();
			dim=m.length;
			for (int j = 0; j < dim - 1; j++) {
				str = str.append(Double.toString(m[j]));
				str = str.append(colDelimiter);
			}
			str = str.append(Double.toString(m[dim - 1]));
			str = str.append("\n");
		}
		return str.toString();
	}
}
