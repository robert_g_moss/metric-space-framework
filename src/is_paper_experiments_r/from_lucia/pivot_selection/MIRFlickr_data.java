/*******************************************************************************
 * Copyright (c) 2013, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_r.from_lucia.pivot_selection;

import is_paper_experiments_r.from_lucia.FeaturesCollectorsArchives;
import it.cnr.isti.vir.features.AbstractFeaturesCollector;
import it.cnr.isti.vir.features.Floats;
import it.cnr.isti.vir.file.ArchiveException;
import it.cnr.isti.vir.file.FeaturesCollectorsArchive;
import it.cnr.isti.vir.file.FeaturesCollectorsArchive_Buffered;
import it.cnr.isti.vir.id.AbstractID;
import it.cnr.isti.vir.id.IDString;
import it.cnr.isti.vir.similarity.ISimilarity;
import it.cnr.isti.vir.similarity.metric.FloatsL2Metric_L2Norm;
import it.cnr.isti.vir.util.RandomOperations;
import it.cnr.isti.vir.util.TimeManager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import supermetrics.SimplexND;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;


/**
 * @author Lucia
 *
 */
public class MIRFlickr_data{
	public static String in_path="E:/Datasets/MIRFlickr/data/"; //set input path
	public static String out_path="E:/Datasets/MIRFlickr/data/nSimplex/2/";//set output path
	
	public static String feature_path=in_path+"DeepFeatures/"; //set this

	static String ind_clusterFN="E:/Datasets/MIRFlickr/IND_clusters.txt";
	
	static String relustring="ReLu";//noRelu
	//List<CartesianPoint>
	static int maxRO=2;
	static String f_archiveFN=feature_path+"hybrid_gmean_"+relustring+"_fc6L2nomalized/hybrid_gmean_"+relustring+"_fc6L2nomalized.dat";
	static String pivotFileName=out_path+relustring+maxRO+"_random_nRO_selectedFromTheDataset_NOnearDuplicate.dat";
	static String out_FN=out_path+"nearDuplicates_MIRFlickr_hybrid_gmean_"+relustring+"_fc6L2nomalized_EuclideanDist.txt";
	
	static int simplex_dim= 2;
	static ISimilarity sim= new FloatsL2Metric_L2Norm();

	
	
	public static List<CartesianPoint> selectData(int ndata) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException, ArchiveException{
		List<CartesianPoint>  data= new ArrayList<CartesianPoint>();
		FeaturesCollectorsArchive fca =new FeaturesCollectorsArchive(f_archiveFN);
		RandomOperations.setSeed(1987);
		AbstractID[] all_ids=fca.getIDs();
		RandomOperations.getDistinctInts(ndata, all_ids.length);
		for(int i1=0; i1<ndata; i1++) {
			CartesianPoint p1=new CartesianPoint(fca.get(all_ids[i1]).getFeature(Floats.class).values);
			data.add(p1);
		}	
		return data;
	}
	
	public static void saveDuplicateDistances(List<CartesianPoint>  refs ,Metric<CartesianPoint> metric,int npairs, File file) throws Exception{
		Metric<CartesianPoint> l2 = new Euclidean<>();
		String colDelimiter=";";
		System.out.print("Computing distances for random duplicate pairs ..");
		File ind_filename=new File(ind_clusterFN);
		ArrayList<String[]> ind_duplicates= new ArrayList<String[]>();
		if(Files.exists(Paths.get(ind_clusterFN))) { 	
			try (Stream<String> streamlines = Files.lines(Paths.get(ind_clusterFN),StandardCharsets.UTF_8)) {
				streamlines.forEach(line->selectpairs(line,ind_duplicates));
				int npairs_TRUE= ind_duplicates.size();
				if(npairs>npairs_TRUE || npairs<0)
					npairs= npairs_TRUE;
				FeaturesCollectorsArchive fca =new FeaturesCollectorsArchive(f_archiveFN);
				AbstractID[] all_ids=fca.getIDs();
				ArrayList<AbstractID> all_ids_arr=fca.getIDsArrayList();
				int[] ids_random_pair=RandomOperations.getDistinctInts(npairs, npairs_TRUE);
				System.out.print("Selecting npairs: "+ npairs);
				String header="actualDist"+colDelimiter+"2simplex\n";
				FileUtil.saveFile(file,header,false);
				SimplexND<CartesianPoint> simplex = new SimplexND<>(2, metric, refs);
				StringBuilder str = new StringBuilder(npairs*2);
				for(int i1=0; i1<npairs; i1++) {
					String[] pair=ind_duplicates.get(ids_random_pair[i1]);
					AbstractID id1=new IDString(pair[0]);
					AbstractID id2=new IDString(pair[1]);
					AbstractFeaturesCollector curr1=fca.get(id1);
					AbstractFeaturesCollector curr2=fca.get(id2);
					Floats first_Float=(curr1).getFeature(Floats.class);
					Floats second_Float=(curr2).getFeature(Floats.class);
					CartesianPoint p1=new CartesianPoint(first_Float.values);
					CartesianPoint p2=new CartesianPoint(second_Float.values);
					double origDist=metric.distance(p1, p2);
					double[] x1=simplex.formSimplex(p1);
					double[] x2=simplex.formSimplex(p2);
					double projectedDist=l2.distance(new CartesianPoint(x1), new CartesianPoint(x2));
					str = str.append(Double.toString(origDist));
					str = str.append(colDelimiter);
					str = str.append(Double.toString(projectedDist));
					str = str.append("\n");
				}
					
				//TimeManager tm = new TimeManager(ind_duplicates.size());tm.reportProgress();
				
				fca.close();
				FileUtil.saveFile(file,str.toString(),true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	//	
	//	

	public static  void pivotsSelection(FeaturesCollectorsArchive in_fca, String pivotArchive_FN, int[] ids_forPivots) throws Exception {
		FeaturesCollectorsArchive_Buffered fcaOut =FeaturesCollectorsArchive_Buffered.createAs(pivotArchive_FN, in_fca);
		System.out.println("Selecting and saving "+ids_forPivots.length+" random pivots..");
		TimeManager tm = new TimeManager();
		tm.setTotNEle(ids_forPivots.length);
		for ( int i : ids_forPivots ) {
			tm.reportProgress();
			fcaOut.add(in_fca.get(i));
		}
		fcaOut.close();
		System.out.println("done");
	}

	private static void selectpairs(String line, ArrayList<String[]> duplicates) {
		String[] splitted =line.split("\\s+");
		int splitsize=splitted.length;				
		for(int i1=0; i1<splitsize; i1++) {
			for(int i2=i1+1; i2<splitsize; i2++) {
				String[] newpair={splitted[i1],splitted[i2]};
				//				System.out.println(splitted[i1]+" -- "+splitted[i2]);
				duplicates.add(newpair);
			}
		}
	}
}
