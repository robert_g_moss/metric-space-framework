package is_paper_experiments_r.from_lucia.pivot_selection;

import it.cnr.isti.vir.similarity.ISimilarity;
import it.cnr.isti.vir.similarity.metric.FloatsL2Metric;
import it.cnr.isti.vir.util.RandomOperations;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import supermetrics.SimplexND;
import testloads.TestLoad;
import testloads.TestLoad.SisapFile;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
/**
* Example of pivot selection. 
* <br>It uses the VIR library.
*
* 
* @author Lucia
*
*/
public class TestPivotSelection {
	static 	String[] selectionMethods= {"kmeans"}; //set selection method
	//option:
	//"randomData"
	//"kmeans"
	// "kmedoid"
	//"fft",
	static int k=2; //set number of pivots
	static String outpath="MATLAB/LuciaEXP/";		
	static 	String[] datasets= {"colors"}; //data sets to be used
	//"MirFlickr"
	//"nasa"
	//"colors"
	static int maxNObjs=50000; //maximum number of object to be used in the kmeans , set to -1 to use all the data objects
	static int npairs=5000;//# random couples of point to save the distances (before and after) the 2-simplex projection
	/**
	 * 
	 * -test several pivot selection methods;<br>
	 * -save the corresponding 2-simplex projections in a txt file (to be used for plotting the data in matlab , e.g. using /MATLAB/LuciaEXP/scatterplot_datapoint.m);<br>
	 * -randomly select 5000 pairs of data points  and for each
	 * pair save the distances before and after the 2-simplex projection into a txt file (to be used for plotting the data in matlab, e.g. using /MATLAB/LuciaEXP/distance_correlationPlot.m)
	 * @param a
	 * @throws Exception
	 */
	public static void main(String[] a) throws Exception{
		
		for(String dataset:datasets) {
			List<CartesianPoint> data=null;
			SisapFile sFile;
			TestLoad tl;
			switch (dataset) {
				case "nasa" :
					System.out.println("Using SISAP nasa dataset");
					sFile= TestLoad.SisapFile.nasa;
					tl = new TestLoad(sFile);
					data = tl.getDataCopy();
					break;
				case "colors":
					System.out.println("Using SISAP color dataset");
					sFile= TestLoad.SisapFile.colors;
					tl = new TestLoad(sFile);
					data = tl.getDataCopy();
					break;
				case "MirFlickr":
					System.out.println("Using MirFlickr (CNN features) dataset");
					data=MIRFlickr_data.selectData(maxNObjs);
					break;		
				default :
					break;
			}
			

			List<CartesianPoint> referencePoints;
			boolean medoids=false;
			ISimilarity vir_metric; //VIR class to be used for k-means
			double distRedThr; //VIR kmeans parameter
			int nIterations; ////k-means is computed nIterations times and the solution with minimum distortion is considered 
			int maxMinPerIteration;//VIR kmeans parameter
			for(String selMethod: selectionMethods) {					
				switch (selMethod) {
					case "kmedoid" :
						System.out.println("***running kmedoid***");
						vir_metric= new FloatsL2Metric();
						//example of vir_metric to be used with Floats are:
						//FloatsL2Metric
						//FloatsJSDMetric (); 
						medoids = true;
						 distRedThr = 0.000001; 
						 nIterations = 6;
						 maxMinPerIteration = 120;
						referencePoints=PivotSelection.kmeansFloats(data,k,medoids,outpath,maxNObjs,vir_metric,distRedThr,nIterations,maxMinPerIteration);
						break;
					case "kmeans" :
						System.out.println("***running kmeans***");
						medoids = false;
						vir_metric= new FloatsL2Metric();
						//example of vir_metric to be used with Floats are:
						//FloatsL2Metric
						//
						 distRedThr = 0.000001; 
						 nIterations = 6;
						 maxMinPerIteration = 120;
						vir_metric = new FloatsL2Metric();
						referencePoints=PivotSelection.kmeansFloats(data,k,medoids,outpath,maxNObjs,vir_metric,distRedThr,nIterations,maxMinPerIteration);
						break;
					case "randomData":
						System.out.println("***running random (pivots are selected from the dataset)***");
						referencePoints= PivotSelection.random(data,k);
						break;
					case "fft":
						System.out.println("***running fft***");
						referencePoints= PivotSelection.fft(data,new Euclidean<>(),k);
						break;
					default :	
						////***RANDOM SELECTION***
						System.out.println("***running random (pivots are selected from the dataset)***");
						referencePoints= PivotSelection.random(data,k);
						break;							
						//							System.out.println("***running random (random double)***");
						//							boolean pivotFromDataset = false; 
						//							referencePoints= PivotSelection.random(data,k,pivotFromDataset);
						//							break;
				}

				//***simplex projection
				Metric<CartesianPoint> metric = new Euclidean<>();
				//List<CartesianPoint> projected_data= simplexProjection(data, referencePoints,metric);
				double[][] projected_dataArr= simplexProjectionDoubleArr(data, referencePoints,metric);

				//save projected_dataArr data for matlab 
				FileUtil.saveFile(new File(outpath+dataset+"_"+selMethod+"-result.txt"), FileUtil.doubleArrayToString(projected_dataArr));			
				FileUtil.saveFile(new File(outpath+dataset+"_"+selMethod+"-refs.txt"),FileUtil.ListCartesianPointToString(referencePoints));
				String npairS=npairs+"";
				if(npairs<0)
					npairS="all";
				String out_FN=outpath+dataset+"_"+selMethod+"-npairs_"+npairS+"_dist.txt"; //first column original dist, second column dist after projection
				saveDistancesRandomPairs(data,referencePoints,metric,npairs,new File(out_FN));
				
				if(dataset.equals("MirFlickr")) {
					npairS="all";
					String out_dup=outpath+dataset+"_"+selMethod+"-DUPLICATEnpairs_"+npairS+"_dist.txt"; //first column original dist, second column dist after projection
					MIRFlickr_data.saveDuplicateDistances(referencePoints,metric,-1,new File(out_dup));
				}
				

			}

		}

			}
	/**
	 * @param data
	 * @param metric 
	 * @param referencePoints
	 * @param npairs
	 * @param file
	 * @throws Exception 
	 */
	private static void saveDistancesRandomPairs(List<CartesianPoint> data,
			List<CartesianPoint> refs, Metric<CartesianPoint> metric, int npairs, File file) throws Exception {
		Metric<CartesianPoint> l2 = new Euclidean<>();
		String colDelimiter=";";
		System.out.print("Computing distances for random pairs ..");
		int n_data=data.size();
		int no_dims= refs.size();
		RandomOperations.setSeed(1987);
		SimplexND<CartesianPoint> simplex = new SimplexND<>(no_dims, metric, refs);
		if(npairs*2>n_data ||npairs<0)
			npairs=n_data/2;
		int[] ids_obj_couple=RandomOperations.getDistinctInts(2*npairs, data.size());
		String header="actualDist"+colDelimiter+"2simplex\n";
		FileUtil.saveFile(file,header,false);
		StringBuilder str = new StringBuilder(npairs*2);
		for(int i1=0; i1<npairs; i1++) {
			CartesianPoint p1=data.get(ids_obj_couple[i1]);
			CartesianPoint p2=data.get(ids_obj_couple[i1+npairs]);
			double origDist=metric.distance(p1, p2);
			double[] x1=simplex.formSimplex(p1);
			double[] x2=simplex.formSimplex(p2);
			double projectedDist=l2.distance(new CartesianPoint(x1), new CartesianPoint(x2));
			str = str.append(Double.toString(origDist));
			str = str.append(colDelimiter);
			str = str.append(Double.toString(projectedDist));
			str = str.append("\n");
		}
		FileUtil.saveFile(file,str.toString(),true);
		System.out.println("done");		
		
	}
	private static List<CartesianPoint>simplexProjection(List<CartesianPoint> data, List<CartesianPoint> refs, Metric metric) throws Exception {
		int no_dims= refs.size();
		SimplexND<CartesianPoint> simplex = new SimplexND<>(no_dims, metric, refs);
		List<CartesianPoint> outList=new ArrayList<CartesianPoint>();
		
		//project point
		for(CartesianPoint x: data) {
			double[] y=simplex.formSimplex(x);
			outList.add(new CartesianPoint(y));
		}			
		return outList;
		
	}
	
	private static double[][] simplexProjectionDoubleArr (List<CartesianPoint> data, List<CartesianPoint> refs, Metric metric) throws Exception {
		int n_data=data.size();
		int no_dims= refs.size();
		double[][] y=new double[n_data][no_dims];
		SimplexND<CartesianPoint> simplex = new SimplexND<>(no_dims, metric, refs);
		
		
		//project point
		for(int i1=0; i1<n_data; i1++) {
			y[i1]=simplex.formSimplex(data.get(i1));
		}			
		return y;
		
	}
	
	
	
	
}
