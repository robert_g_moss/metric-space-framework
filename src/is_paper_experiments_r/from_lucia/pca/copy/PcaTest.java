/*******************************************************************************
 * Copyright (c) 2016, Lucia Vadicamo (NeMIS Lab., ISTI-CNR, Italy)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package is_paper_experiments_r.from_lucia.pca.copy;

import is_paper_experiments_l.MathUtil;

import java.util.ArrayList;
import java.util.List;

import testloads.TestContext;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;

/**
 * @author Lucia
 *
 */
public class PcaTest {
	public static void main(String[] args) throws Exception {
		
		test();
	//	testcolors();
		
		
		
		
	}

	/**
	 * @throws Exception 
	 * 
	 */
	private static void test() throws Exception {

		/** Training data matrix with each row corresponding to data point and
	        * each column corresponding to dimension. */
		double[][]  trainingData = new double[][] {
	            {1, 2, 3, 4, 5, 6},
	            {6, 5, 4, 3, 2, 1},
	            {2, 2, 2, 2, 2, 2}};
		
		List<CartesianPoint> data4PCA =new ArrayList<CartesianPoint>();
		
		for(int i1=0; i1<trainingData.length; i1++) {
			double[] p=trainingData[i1];
			data4PCA.add(new CartesianPoint(p) );
			
		}
		
		PCAviaSVD pca= new PCAviaSVD(data4PCA);
		PrincipalComponents pc=pca.computePCA();
		
		double[][] rotationMatrix=pc.getEigenVectors();//eigenVec stored in row 
		double[] eigenValues=pc.getEigenValues();
		double[] means=pc.getMeans();
		
		double all_variance=MathUtil.sum(eigenValues);	
		
		//double[] firstPC=eigenVectors[0];
		double explained_variance=eigenValues[0]/all_variance*100;
		
	
		
		System.out.println("number of eigenvalue greater than 10^-16: " +eigenValues.length);
		System.out.println("percentage of the total variance explained by the 1st principal component: "+explained_variance);
		
		//double[] lastPC=eigenVectors[eigenVectors.length-1];
		explained_variance=eigenValues[eigenValues.length-1]/all_variance*100;
		System.out.println("percentage of the total variance explained by the last principal component: "+explained_variance);
		
		//rotate and eventually reduce the data 
		List<CartesianPoint> centeredRotatedData=new ArrayList<CartesianPoint>();
		
		System.out.println("new Data");
		for(CartesianPoint cp: data4PCA) {
			double[] p=cp.getPoint();
			int d1=rotationMatrix.length;
			int d2=rotationMatrix[0].length;
			double[] rp=new double[d1];
			
			for (int i1=0; i1<d1; i1++) {
				double sum=0;
				for (int i2=0; i2<d2; i2++) {
					double val=p[i2]-means[i2];
					sum+=rotationMatrix[i1][i2]*val;
				}
				rp[i1]=sum;
				System.out.print(sum+" ; ");
				}
			System.out.println();
			centeredRotatedData.add(new CartesianPoint(rp));
			
		}
		
		
		
		
	}

	/**
	 * @throws Exception 
	 * 
	 */
	private static void testcolors() throws Exception {
		// TODO Auto-generated method stub
		int k=2; //set number of pivots
		int maxNObjs=30000;//maximum number of points to be considering in learning the pca matrix
		
		String outpath="MATLAB/LuciaEXP/";		

		
		System.out.println("Using SISAP nasa dataset");
		TestContext tc = new TestContext(TestContext.Context.colors);
		Metric metric=tc.metric();
		List<CartesianPoint> data = tc.getData();	
			
		
/////////////if 2-simplex///////////////
//		
//		//select k reference objects
//		System.out.println("***running random (pivots are selected from the dataset)***");
//		List<CartesianPoint> referencePoints= PivotSelection.random(data,k);
//		//
//		
//		//projecting 2-simplex
//		SimplexND<CartesianPoint> simplex = new SimplexND<>(k, metric, referencePoints);
//		List<CartesianPoint> projPoints=new ArrayList<CartesianPoint>();
//		for(CartesianPoint p:data) {
//			CartesianPoint apex=new CartesianPoint(simplex.formSimplex(p));
//			projPoints.add(apex);
//		}
//		data=projPoints;
////////////////////////	
		
		List data4PCA=data;
		
		//select a random subset of the data 
		if(data.size()>maxNObjs){
			System.out.println("Sececting "+maxNObjs+" from "+data.size()+" data objects");
			ArrayList<Integer> list=new ArrayList<Integer>();
			for(int i1=0; i1<data.size(); i1++) {
				list.add(i1);
			}
			java.util.Collections.shuffle(list);
			
			List<Integer> list2=list.subList(0, maxNObjs);
			
			data4PCA=new ArrayList ();
			for(Integer i1: list2) {
				data4PCA.add(data.get(i1));
			}
			
		}
		// end selection points/
		
		PCAviaSVD pca= new PCAviaSVD(data4PCA);
		PrincipalComponents pc=pca.computePCA();
		
		double[][] rotationMatrix=pc.getEigenVectors();//eigenVec stored in row 
		double[] eigenValues=pc.getEigenValues();
		double[] means=pc.getMeans();
		
		double all_variance=MathUtil.sum(eigenValues);	
		
		//double[] firstPC=eigenVectors[0];
		double explained_variance=eigenValues[0]/all_variance*100;
		System.out.println("number of eigenvalue greater than 10^-16: " +eigenValues.length);
		System.out.println("percentage of the total variance explained by the 1st principal component: "+explained_variance);
		
		//double[] lastPC=eigenVectors[eigenVectors.length-1];
		explained_variance=eigenValues[eigenValues.length-1]/all_variance*100;
		System.out.println("percentage of the total variance explained by the last principal component: "+explained_variance);
		
		//rotate data
		List<CartesianPoint> centeredRotatedData=new ArrayList<CartesianPoint>();
		
		for(CartesianPoint cp: data) {
			double[] p=cp.getPoint();
			int d1=rotationMatrix.length;
			int d2=rotationMatrix[0].length;
			double[] rp=new double[d2];
			
			for (int i1=0; i1<d1; i1++) {
				double sum=0;
				for (int i2=0; i2<d2; i2++) {
					double val=p[i2]-means[i2];
					sum+=rotationMatrix[i1][i2]*val;
				}
				rp[i1]=sum;
				}
			centeredRotatedData.add(new CartesianPoint(rp));
			
		}
	}
}
