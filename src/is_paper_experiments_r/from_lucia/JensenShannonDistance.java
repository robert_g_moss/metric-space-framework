package is_paper_experiments_r.from_lucia;

/**
 * @author Lucia Vadicamo Jensen Shannon divergence is not metric The
 *         square-root of the JSDiv is metric
 * 
 */

public class JensenShannonDistance {
	private static final double LOG_2 = Math.log(2);

	/**
	 * compute the square-root of the J-S Divergence (logs are taken to base two
	 * so that the outcome is bounded in [0,1])
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static final double get(float[] f1, float[] f2) {
		Normalize.l1(f1, true);
		Normalize.l1(f2, true);
		return Math.sqrt(getSquared(f1, f2));
	}

	public static final double get(float[] f1, float[] f2, double maxDist) {
		Normalize.l1(f1, true);
		Normalize.l1(f2, true);
		double dist = Math.sqrt(getSquared(f1, f2));
		if (dist > maxDist)
			return -dist;
		else
			return dist;

	}

	/**
	 * compute the square-root of the J-S Divergence (logs are taken to base two
	 * so that the outcome is bounded in [0,1])
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static double get(double[] f1, double[] f2) {
		Normalize.l1(f1, true);
		Normalize.l1(f2, true);
		return Math.sqrt(getSquared(f1, f2));
	}

	public static final double get(double[] f1, double[] f2, double maxDist) {
		Normalize.l1(f1, true);
		Normalize.l1(f2, true);
		double dist = Math.sqrt(getSquared(f1, f2));
		if (dist > maxDist)
			return -dist;
		else
			return dist;

	}

	/**
	 * compute the J-S Divergence (logs are taken to base two so that the
	 * outcome is bounded in [0,1])
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static final double getSquared(float[] f1, float[] f2) {
		double dist = 0;
		for (int i = 0; i < f1.length; i++) {
			double f1_i = f1[i];
			if (f1_i != 0) {
				double f2_i = f2[i];
				if (f2_i != 0) {
					double sum = f1_i + f2_i;
					double logsum = Math.log(sum);
					dist += sum * logsum;

					dist -= f1_i * (Math.log(f1_i));
					dist -= f2_i * (Math.log(f2_i));
				}
			}
		}
		dist = dist / (2 * LOG_2);
		return 1 - dist;
	}

	/**
	 * compute the J-S Divergence (logs are taken to base two so that the
	 * outcome is bounded in [0,1])
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static double getSquared(double[] f1, double[] f2) {
		double dist = 0;
		for (int i = 0; i < f1.length; i++) {
			double f1_i = f1[i];
			if (f1_i != 0) {
				double f2_i = f2[i];
				if (f2_i != 0) {
					double sum = f1_i + f2_i;
					double logsum = Math.log(sum);
					dist += sum * logsum;

					dist -= f1_i * (Math.log(f1_i));
					dist -= f2_i * (Math.log(f2_i));
				}
			}
		}
		return 1 - dist / (2 * LOG_2);
	}

}