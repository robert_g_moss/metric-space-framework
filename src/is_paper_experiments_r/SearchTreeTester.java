package is_paper_experiments_r;

import is_paper_experiments_r.binary_exclusions_OLD_DONT_USE.BinaryExclusionFactory;
import is_paper_experiments_r.binary_exclusions_OLD_DONT_USE.ExhaustiveSearchTree;
import is_paper_experiments_r.binary_exclusions_OLD_DONT_USE.HyperplaneTree;
import is_paper_experiments_r.binary_exclusions_OLD_DONT_USE.SearchTree;
import is_paper_experiments_r.binary_exclusions_OLD_DONT_USE.VantagePointTree;
import is_paper_experiments_r.binary_partitions.RandomPartition;
import is_paper_experiments_r.binary_partitions.SimpleWidePartition;

import java.util.ArrayList;
import java.util.List;

import searchStructures.SearchIndex;
import testloads.TestContext;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;

/**
 * 
 * utility class that is likely to change as different experiments are run
 * 
 * @author Richard Connor
 *
 */
public class SearchTreeTester {

	private static List<BinaryExclusionFactory<CartesianPoint>> getMechanisms(
			TestContext tc, Metric<CartesianPoint> cm) {

		List<BinaryExclusionFactory<CartesianPoint>> res = new ArrayList<>();
		// add exhaustive search to benchmark
		res.add(new ExhaustiveSearchTree<>(cm));

		// add a VPT exclusion mechanism
		res.add(new VantagePointTree<>(cm));

		// add a HPT using the four point property and random reference point
		// selection
		final HyperplaneTree<CartesianPoint> hpt1 = new HyperplaneTree<>(cm);
		hpt1.setFourPoint(true);
		hpt1.setPartitionStrategy(new RandomPartition<>(cm));
		res.add(hpt1);

		// add a HPT using the four point property and simple, widely spaced
		// reference point selection
		final HyperplaneTree<CartesianPoint> hpt2 = new HyperplaneTree<>(cm);
		hpt2.setFourPoint(true);
		hpt2.setPartitionStrategy(new SimpleWidePartition<>(cm));
		res.add(hpt2);

		return res;
	}

	/**
	 * kick off whatever is currently here...!
	 * 
	 * @param a
	 *            not used
	 * @throws Exception
	 *             if test context can't be created
	 */
	public static void main(String[] a) throws Exception {
		try {
			assert false;
			System.out.println("assertions not enabled");
		} catch (Throwable t) {
			System.out.println("assertions enabled");
		}

		int noOfQueries = 100;
		TestContext tc = new TestContext(100 * 1000 + noOfQueries);
		// TestContext tc = new TestContext(TestContext.Context.colors);
		tc.setSizes(noOfQueries, 0);

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		List<BinaryExclusionFactory<CartesianPoint>> excMechs = getMechanisms(
				tc, cm);

		cm.reset();

		for (BinaryExclusionFactory<CartesianPoint> mech : excMechs) {

			SearchIndex<CartesianPoint> tree = new SearchTree<>(tc.getDataCopy(),
					mech);

			System.out.println("mechanism: " + tree.getShortName());
			// System.out.println("data size: " + tree.cardinality());
			List<CartesianPoint> res = new ArrayList<>();
			cm.reset(); // actually unnecessary but future-proofed!

			for (CartesianPoint q : tc.getQueries()) {
				res.addAll(tree.thresholdSearch(q, tc.getThreshold()));
			}
			System.out.println("results size " + res.size());
			System.out.println("mean distances per query: "
					+ (cm.reset() / noOfQueries));
		}

		System.out.println("done");
	}
}
