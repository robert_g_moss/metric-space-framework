package is_paper_experiments_r.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Measurements {
	private Map<String, List<Integer>> measurements;

	public Measurements(String... names) {
		this.measurements = new TreeMap<>();
		for (String s : names) {
			this.measurements.put(s, new ArrayList<Integer>());
		}
	}

	public void addCount(String name, int number) {
		try {
			this.measurements.get(name).add(number);
		} catch (Throwable t) {
			throw new RuntimeException("wrong name used for measurement");
		}
	}

	public double getMean(String name) {
		double acc = 0;
		for (int i : measurements.get(name)) {
			acc += i;
		}
		return acc / measurements.get(name).size();
	}

	public double getSD(String name) {
		double mean = getMean(name);
		double acc = 0;
		for (int i : measurements.get(name)) {
			acc += (mean - i) * (mean - i);
		}
		return Math.sqrt(acc / (measurements.get(name).size() + 1));
	}

	public double getStdErrorOfMean(String name) {
		double std = getSD(name);
		return std / Math.sqrt(measurements.get(name).size());
	}
}