/**
 * 
 * A package to contain different exclusion mechanisms for use with the binary tree class SearchTree
 */
/**
 * 
 * @author Richard Connor
 *
 */
package is_paper_experiments_r.binary_tree_search;