/**
 * this package aims to create (and recreate!) experiments performed for the paper "Supermetric 
 * Search and the Four-Point Property" published in SISAP16, with an invited extension to the 
 * Information Systems journal
 */
/**
 * @author Richard Connor
 *
 */
package is_paper_experiments_r;