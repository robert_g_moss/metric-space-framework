package is_paper_experiments_r.four_point_surrogate;

import is_paper_experiments_r.binary_partitions.BinaryPartitionFactory;
import is_paper_experiments_r.binary_partitions.SimpleWidePartition;
import is_paper_experiments_r.binary_tree_search.MonotoneHyperplaneTree;
import is_paper_experiments_r.binary_tree_search.SearchTree;
import is_paper_experiments_r.util.Range;
import is_paper_experiments_r.util.Util;

import java.util.List;

import searchStructures.SearchIndex;
import searchStructures.VPTree;
import testloads.TestContext;
import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import coreConcepts.NamedObject;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;

public class SurrogateTester {

	public static void main(String[] args) throws Exception {

		try {
			assert false;
			System.out.println("assertions off");
		} catch (Throwable e) {
			System.out.println("assertions on");
		}

		doTest();

		System.out.println("done");
	}

	private static void doTest() throws Exception {
		TestContext tc = new TestContext(TestContext.Context.colors);
		assert tc != null;
		final Metric<CartesianPoint> metric = new Euclidean<>();
		CountedMetric<CartesianPoint> originalMetric = new CountedMetric<>(
				metric);
		CountedMetric<CartesianPoint> recheckMetricT = new CountedMetric<>(
				metric);
		CountedMetric<CartesianPoint> recheckMetricF = new CountedMetric<>(
				metric);
		CountedMetric<CartesianPoint> recheckMetricN = new CountedMetric<>(
				metric);

		tc.setSizes((tc.getDataCopy().size()) / 10 - 1000, 1000);
		List<CartesianPoint> refs = Util.getFFT(tc.getRefPoints(), metric, 123);
		// List<CartesianPoint> refs = Util.getRandom(tc.getRefPoints(), 10);
		// List<CartesianPoint> refs = tc.getRefPoints();
		// System.out.println(Util.minPivot(refs, metric));
		// System.out.println(Util.maxPivot(refs, metric));

		// SurrogateSpaceCreator<CartesianPoint> sscF = new
		// SurrogateSpaceCreator<>(
		// tc.getDataCopy(), recheckMetricF,
		// SurrogateSpaceCreator.Type.fourPoint);
		SurrogateSpaceCreator<CartesianPoint> sscT = new SurrogateSpaceCreator<>(
				tc.getDataCopy(), recheckMetricT,
				SurrogateSpaceCreator.Type.threePoint);
		SurrogateSpaceCreator<CartesianPoint> sscN = new SurrogateSpaceCreator<>(
				tc.getDataCopy(), recheckMetricN,
				SurrogateSpaceCreator.Type.nPoint);
		sscT.addData3p(refs);
		// sscF.addData4p(refs);
		sscN.addDataNp(refs);
		recheckMetricF.reset();
		recheckMetricT.reset();
		recheckMetricN.reset();

		BinaryPartitionFactory<CartesianPoint> sw = new SimpleWidePartition<>(
				originalMetric);
		final MonotoneHyperplaneTree<CartesianPoint> mhpt = new MonotoneHyperplaneTree<>(
				originalMetric);
		mhpt.setFourPoint(true);
		mhpt.setPartitionStrategy(sw);

		SearchIndex<CartesianPoint> vpt = new SearchTree<>(tc.getDataCopy(),
				mhpt);
		//
		// SearchIndex<CartesianPoint> vpt = new VPTree<>(tc.getDataCopy(),
		// originalMetric);
		originalMetric.reset();

		final List<CartesianPoint> queries = tc.getQueries();
		int xres = 0;
		int yres = 0;
		int zres = 0;
		int z1res = 0;
		for (int q : Range.range(0, queries.size())) {
			final CartesianPoint query = queries.get(q);
			final double threshold = tc.getThreshold();
			// final double threshold = 0.4;
			// System.out.println(threshold);
			List<CartesianPoint> x = vpt.thresholdSearch(query, threshold);
			List<CartesianPoint> y = sscT.thresholdSearch(query, threshold);
			// List<CartesianPoint> z = sscF.thresholdSearch(query, threshold);
			List<CartesianPoint> z1 = sscN.thresholdSearch(query, threshold);
			xres += x.size();
			yres += y.size();
			// zres += z.size();
			z1res += z1.size();
		}

		System.out.println(xres + ";" + yres + ";" + zres + ";" + z1res);

		final int qSize = queries.size();

		System.out.println("orginal:" + originalMetric.reset() / qSize);
		System.out.println("recheck3p:" + recheckMetricT.reset() / qSize);
		System.out.println("recheck4p:" + recheckMetricF.reset() / qSize);
		System.out.println("recheckNp:" + recheckMetricN.reset() / qSize);
		System.out.println("surrogate3p:" + sscT.countMetricCalls() / qSize);
		// System.out.println("surrogate4p:" + sscF.countMetricCalls() / qSize);
		System.out.println("surrogateNp:" + sscN.countMetricCalls() / qSize);
	}

	private static void measureEtc(TestContext tc,
			CountedMetric<CartesianPoint> originalMetric,
			CountedMetric<CartesianPoint> recheckMetric,
			SurrogateSpaceCreator<CartesianPoint> ssc) {

	}
}
