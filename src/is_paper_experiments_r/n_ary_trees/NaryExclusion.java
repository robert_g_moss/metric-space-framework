package is_paper_experiments_r.n_ary_trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Richard Connor
 *
 * @param <T>
 */
/**
 * 
 * 
 * This class (maybe should be an interface, but may start to include utility
 * functions) is the one available to a generic metric index implementation,
 * which allows the logical separation of creation, testing etc from the actual
 * exclusion mechanisms being used
 * 
 * use: an ExclusionFactory class is used by a generic data arrangement
 * mechanism (eg a binary search tree in the simplest case) to generate an
 * exclusion according to the given context, ie the data and metric available
 * 
 * 
 * @author Richard Connor
 *
 * @param <T>
 *            the type of data being searched
 * @param <CreationInfoType>
 *            the type of info required from the parent context when a child
 *            node is created. For example in a monotone tree it is the shared
 *            reference point
 * @param <QueryInfoType>
 *            the type of info required from the parent context when a child
 *            node is queried; for example in a monotone tree it is the distance
 *            to the shared reference point
 */
public abstract class NaryExclusion<T, CreationInfoType, QueryInfoType> {

	NaryExclusion(List<T> data, CreationInfoType creationInfo) {
		if (creationInfo == null) {
		}
	}

	/**
	 * result when a query is applied to a given tree node
	 * 
	 * @author Richard Connor
	 *
	 */
	public class QueryResult {

		private List<T> newResults;
		private boolean[] exclusions;
		private List<QueryInfoType> queryContexts;

		protected QueryResult(int arity) {
			this.newResults = new ArrayList<>();
			this.queryContexts = new ArrayList<>();
			for (int i = 0; i < arity; i++) {
				queryContexts.add(null);
			}
			this.exclusions = new boolean[arity];

		}

		public boolean[] getExclusions() {
			return this.exclusions;
		}

		public List<T> getResults() {
			return this.newResults;
		}

		public List<QueryInfoType> getContexts() {
			return this.queryContexts;
		}

		protected void setExclusion(int exc) {
			this.exclusions[exc] = true;
		}

		protected void setQueryContext(int which, QueryInfoType qi) {
			this.queryContexts.set(which, qi);
		}

		protected void addResult(T result) {
			this.newResults.add(result);
		}
	}

	/**
	 * must be called before either excludeLeft or excludeRight at each node
	 * 
	 * @param m
	 */
	public abstract QueryResult getQueryInfo(T query, double threshold,
			QueryInfoType queryContext);

	/**
	 * 
	 * @return data to be stored in the left subtree, recursively
	 */
	protected abstract List<List<T>> getDataPartitions();

	protected abstract int getArity();

	protected abstract boolean isDataNode();

	protected abstract List<CreationInfoType> getCreationContexts();

	/**
	 * the volume of the original dataset which has been removed from the
	 * recursive search and is therefore now stored only in this object
	 * 
	 * @return
	 */
	public abstract int storedDataSize();

}
