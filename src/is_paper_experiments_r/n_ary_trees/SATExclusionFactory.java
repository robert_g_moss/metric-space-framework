package is_paper_experiments_r.n_ary_trees;

import java.util.ArrayList;
import java.util.List;

import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import coreConcepts.Metric;

public abstract class SATExclusionFactory<T> extends
		NaryExclusionFactory<T, T, Double> {

	public SATExclusionFactory(Metric<T> metric) {
		super(metric);
	}

	private class SATExclusion extends NaryExclusion<T, T, Double> {

		boolean isNullNode;
		boolean isHeadNode;
		boolean isDataNode;
		T centreNode;
		List<T> neighbours;
		List<List<T>> dataSets;
		double[] cr;
		double[][] neighbourDists;

		SATExclusion(List<T> data, T creationInfo) {
			super(data, creationInfo);
			assert false : "shouldn't be calling this any more";

			/*
			 * first, set the head node; either from context, or random choice
			 * if this is the head node
			 */
			if (data.size() == 0) {
				this.isNullNode = true;
			} else {
				if (creationInfo == null) {
					this.isHeadNode = true;
					this.centreNode = data.remove(SATExclusionFactory.this.rand
							.nextInt(data.size()));
				} else {
					this.centreNode = creationInfo;
				}

				// getReferencePoints is contracted to return any non-empty
				// subset of data, it shouldn't side-effect data
				this.neighbours = getReferencePoints(data, this.centreNode);

				// so if all of the data has been consumed into the neighbours,
				// it's a data node
				if (this.neighbours.size() == data.size()) {
					this.isDataNode = true;
				} else {
					this.dataSets = new ArrayList<>();
					for (T p : this.neighbours) {
						data.remove(p);
						this.dataSets.add(new ArrayList<T>());
					}

					/*
					 * create the neighbour distance table for four-point
					 * exclusion
					 */
					this.neighbourDists = new double[neighbours.size()][neighbours
							.size()];
					for (int i = 0; i < neighbours.size() - 1; i++) {
						for (int j = i + 1; j < neighbours.size(); j++) {
							double dist = metric.distance(neighbours.get(i),
									neighbours.get(j));
							neighbourDists[i][j] = dist;
							neighbourDists[j][i] = dist;
						}
					}

					// create array for cover radii
					this.cr = new double[this.neighbours.size()];

					for (T dat : data) {

						// for (T dat : data.subList(ptr, data.size())) {
						int closest = -1;
						double leastDist = Double.MAX_VALUE;
						int cptr = 0;
						for (T n : neighbours) {
							final double dist = metric.distance(n, dat);
							if (dist < leastDist) {
								closest = cptr;
								leastDist = dist;
							}
							cptr++;
						}
						this.dataSets.get(closest).add(dat);
						this.cr[closest] = Math
								.max(leastDist, this.cr[closest]);
					}
				}
			}
		}

		@SuppressWarnings("boxing")
		@Override
		public QueryResult getQueryInfo(T query, double threshold,
				Double queryContext) {
			QueryResult res = new QueryResult(this.getArity());

			if (!this.isNullNode) {
				/*
				 * either calculate, or import from upper node, the distance
				 * from the query to the centre node
				 */
				double qtoHeadDist;
				if (this.isHeadNode) {
					qtoHeadDist = metric.distance(query, this.centreNode);
					if (qtoHeadDist <= threshold) {
						res.addResult(this.centreNode);
					}
				} else {
					qtoHeadDist = queryContext;
				}

				double[] qToNdists = new double[this.neighbours.size()];
				for (int i = 0; i < this.neighbours.size(); i++) {
					T d = this.neighbours.get(i);
					double dist = metric.distance(d, query);

					double minDistToHyperplane = useFourPointProperty() ? Math
							.min(dist, qtoHeadDist) : dist;

					qToNdists[i] = dist;
					res.setQueryContext(i, minDistToHyperplane);

					if (dist <= threshold) {
						res.addResult(d);
					}
				}
				/*
				 * now calculate exclusions
				 */
				if (!this.isDataNode) {
					if (qToNdists[qToNdists.length - 1] > this.cr[qToNdists.length - 1]
							+ threshold) {
						res.setExclusion(qToNdists.length - 1);
					}
					for (int i = 0; i < qToNdists.length - 1; i++) {
						double d1 = qToNdists[i];
						double minD1 = useSatProperty() ? Math.min(qtoHeadDist,
								d1) : d1;
						if (d1 > this.cr[i] + threshold) {
							res.setExclusion(i);
						}
						for (int j = i + 1; j < qToNdists.length; j++) {
							double d2 = qToNdists[j];
							double minD2 = useSatProperty() ? Math.min(
									qtoHeadDist, d2) : d2;

							if (useFourPointProperty()) {
								if ((d1 * d1 - d2 * d2) / neighbourDists[i][j] > 2 * threshold) {
									res.setExclusion(i);
								}
								if ((d2 * d2 - d1 * d1) / neighbourDists[i][j] > 2 * threshold) {
									res.setExclusion(j);
								}
							} else {
								if ((d1 - minD2) > 2 * threshold) {
									res.setExclusion(i);
								}
								if ((d2 - minD1) > 2 * threshold) {
									res.setExclusion(j);
								}
							}
						}
					}
				}
			}
			return res;
		}

		@Override
		protected List<List<T>> getDataPartitions() {
			return this.dataSets;
		}

		@Override
		protected int getArity() {
			if (this.neighbours == null) {
				return 0;
			} else {
				return this.neighbours.size();
			}
		}

		@Override
		protected boolean isDataNode() {
			return this.isDataNode || this.isNullNode;
		}

		@Override
		protected List<T> getCreationContexts() {
			return this.neighbours;
		}

		@Override
		public int storedDataSize() {
			if (this.isNullNode) {
				return 0;
			} else if (this.isHeadNode) {
				return this.neighbours.size() + 1;
			} else {
				return this.neighbours.size();
			}
		}
	}

	@Override
	public NaryExclusion<T, T, Double> getExclusion(List<T> data, T context) {
		return new SATExclusion(data, context);
	}

	abstract protected List<T> getReferencePoints(List<T> data, T centre);

	/**
	 * @return true if, and only if, the metric used has the four-point property
	 */
	abstract protected boolean useFourPointProperty();

	/**
	 * @return true if, and only if, the reference point selection has the SAT
	 *         property; that is, there are no points within the remaining set
	 *         that are closer to the centre point than at least one of the
	 *         reference points
	 */
	abstract protected boolean useSatProperty();

	@Override
	abstract public String getName();

}
