function d = man(A,B)

C = A - B;
C = abs(C);
d = sum(C);

end